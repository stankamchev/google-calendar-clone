import * as types from "./actionTypes";

export const addEvent = (event, date) => ({
  type: types.ADD_EVENT,
  payload: { event, date },
});

export const removeEvent = (date) => ({
  type: types.REMOVE_EVENT,
  payload: date,
});

import * as types from "./actionTypes";

const initialState = {
  events: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_EVENT:
      const newEvent = {
        id: state.events.length,
        event: action.payload.event,
        date: action.payload.date,
      };
      const addedEvent = [...state.events, newEvent];
      return { ...state, events: addedEvent };
    case types.REMOVE_EVENT:
      const removedEvent = state.events.filter(
        (item) => item.id !== action.payload
      );
      return { ...state, events: removedEvent };
    default:
      return state;
  }
};

export default reducer;

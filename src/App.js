import React from "react";
import Navbar from "./Components/Calendar/Calendar";
import Calendar from "./Components/Navbar/Navbar";
function App() {
  return (
    <>
      <Navbar />
      <Calendar />
    </>
  );
}

export default App;

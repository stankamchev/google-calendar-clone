import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() => ({
  cell: {
    height: 150,
    borderBottom: 0,
    maxWidth: 150,
  },
  rowContainer: {
    display: "grid",
    gridTemplateColumns: "repeat(7,1fr)",
    borderBottom: 0,
    padding: 0,
    margin: 0,
  },
  tableCell: {
    borderBottom: 0,
    maxWidth: 150,
    height: 120,
    cursor: "pointer",
  },
  tableContainer: {
    border: 0,
    boxShadow: "none",
  },
  event: {
    backgroundColor: "rgb(11, 128, 67)",
    color: "white",
    marginTop: 30,
    fontSize: 12,
  },
  container:{
    marginTop:100
  }
}));

import React, { useState } from "react";
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@material-ui/core";
import { useStyles } from "./CalendarStyles";
import daysInMonth from "./CalendarData.json";
import { weekDays } from "./CalendarWeek";
import Cell from "../Cell/Cell";

const Calendar = () => {
  const [cellIndex, setCellIndex] = useState(false);
  const onCellClick = (index) => {
    return setCellIndex(index);
  };
  const classes = useStyles();
  return (
    <Container className={classes.container} position="static" maxWidth="md" >
      <TableContainer className={classes.tableContainer} component={Paper}>
        <Table>
          <TableHead>
            <TableRow className={classes.rowContainer}>
              {weekDays.map((day) => {
                return (
                  <TableCell
                    key={day}
                    align="center"
                    className={classes.tableCell}
                  >
                    {day.slice(0, 3)}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className={classes.rowContainer}>
              {daysInMonth.map((day) => {
                return (
                  <TableCell
                    key={day.date}
                    onClick={() => {
                      onCellClick(day.date);
                    }}
                    align="center"
                    component="th"
                    scope="row"
                    className={classes.tableCell}
                  >
                    <Cell date={day.date} cellIndex={cellIndex} />
                  </TableCell>
                );
              })}
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};

export default Calendar;

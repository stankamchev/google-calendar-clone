import React from "react";
import {Typography} from "@material-ui/core";
import Event from "../Event/Event";
import EventForm from "../EventForm/EventForm";
import { useSelector } from "react-redux";
const Cell = ({ date, cellIndex }) => {
  const eventDate = useSelector((state) => state.events);
  return (
    <div>
      <Typography>{date}</Typography>
      {cellIndex === date && <EventForm id={date} />}
      {eventDate.map((event) => {
        return (
          <div key={event}>{date === event.date && <Event date={event} />}</div>
        );
      })}
    </div>
  );
};

export default Cell;

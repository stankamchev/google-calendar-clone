import React, { useState } from "react";
import { Input } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { addEvent } from "../../redux/actions";
import { useStyles } from "./EventFormStyles";
const EventForm = ({ id }) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [text, setText] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    setText("");
    dispatch(addEvent(text, id));
  };

  return (
    <form onSubmit={handleSubmit}>
      <Input
        className={classes.eventInput}
        type="text"
        placeholder="event..."
        value={text}
        disableUnderline={true}
        autoFocus={true}
        onChange={(e) => setText(e.target.value)}
      />
    </form>
  );
};

export default EventForm;

import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() => ({
  eventInput: {
    cursor: "pointer",
    backgroundColor: "rgb(3, 155, 229)",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 3,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 5,
    alignItems: "center",
    color: "white",
    fontSize: 12,
    fontWeight: 500,
  },
}));

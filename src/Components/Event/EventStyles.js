import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() => ({
  eventContainer: {
    cursor: "pointer",
    backgroundColor: "rgb(3, 155, 229)",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 3,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 5,
    alignItems: "center",
  },
  eventTitle: {
    fontWeight: 500,
    fontSize: 12,
    color: "#fff",
  },
  trashIcon: {
    color: "white",
    width: 20,
    height: 20,
  },
}));

import React from "react";
import { Typography } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import { useDispatch } from "react-redux";
import { removeEvent } from "../../redux/actions";
import { useStyles } from "./EventStyles";
const Event = ({ date }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  return (
    <>
      <div className={classes.eventContainer}>
        <Typography className={classes.eventTitle}>{date.event}</Typography>
        {date.event && (
          <i>
            <ClearIcon
              className={classes.eventTitle}
              color="inherit"
              onClick={() => dispatch(removeEvent(date.id))}
            />
          </i>
        )}
      </div>
    </>
  );
};

export default Event;

import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    color: "rgba(0, 0, 0, 0.54)",
    fontWeight: "500",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
    marginRight: 50,
  },
  month: {
    color: "rgba(0, 0, 0, 0.54)",
    fontWeight: "500",
  },
  navContainer: {
    backgroundColor: "#fff",
  },
}));

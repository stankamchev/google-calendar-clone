import React from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Badge,
  Box,
} from "@material-ui/core";
import MailIcon from "@material-ui/icons/Mail";
import MenuIcon from "@material-ui/icons/Menu";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import NotificationsIcon from "@material-ui/icons/Notifications";
import AccountCircle from "@material-ui/icons/AccountCircle";

import { useStyles } from "./NavbarStyles";

const Navbar = () => {
  const classes = useStyles();

  return (
    <Box bgcolor="background.paper" pb={1} className={classes.grow}>
      <AppBar className={classes.navContainer} >
        <Toolbar>
          <IconButton className={classes.menuButton}>
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap>
            Calendar
          </Typography>
          <Typography className={classes.month} variant="h6" noWrap>
            September 2021
          </Typography>
          <div className={classes.grow} />
          <IconButton>
            <Badge color="secondary">
              <MailIcon />
            </Badge>
          </IconButton>
          <IconButton>
            <Badge color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <IconButton>
            <AccountCircle />
          </IconButton>
          <div className={classes.sectionMobile}>
            <IconButton>
              <MoreVertIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
